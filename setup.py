# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import os

import mywebsite_celery
import setuptools
from setuptools.command.sdist import sdist


# Override build command
class BuildCommand(sdist):

    def run(self):
        # Run the original build command
        sdist.run(self)
        # os.system('rm -vrf ./build ./dist ./*.pyc ./*.tgz ./*.egg-info')
        os.system('rm -vrf ./*.egg-info')


with open("README.md", "r") as fh:
    long_description = fh.read()


# returns a list of requirements
def parse_requirements(requirements):
    with open(requirements) as f:
        return [l.strip('\n') for l in f if l.strip('\n') and not (l.startswith('#') or l.startswith('http'))]


# returns a list of dependency_links
def parse_dependency_links(requirements):
    with open(requirements) as f:
        return [l.strip('\n') for l in f if l.strip('\n') and l.startswith('http')]


# uses parse_requirements() to get all depending requirements
reqs = parse_requirements("requirements/production.txt")
links = parse_dependency_links("requirements/production.txt")

setuptools.setup(
    name="mywebsite_celery_django",
    version=mywebsite_celery.__version__,
    author="Chris Daßler",
    author_email="info@dsoft-app-dev.de",
    description="This is the mywebsite_celery package of the modular WEB application for my business. It is written in Python/Django.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://www.dsoft-app-dev.de",
    packages=setuptools.find_packages(exclude=["tests", "test_website"]),
    include_package_data=True,
    install_requires=reqs,
    dependency_links=links,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: German Free Software License 1.0",
        "Operating System :: OS Independent",
    ],
    license='D-FSL-1.0',
    cmdclass={
        'sdist': BuildCommand,
    }
)
