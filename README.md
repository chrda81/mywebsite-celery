# MyWebsite-celery

This is the celery package of the modular WEB application for my business. It is written in Python/Django.

***
License agreement:

Copyright (C) 2018 dsoft-app-dev.de and friends.

This Program may be used by anyone in accordance with the terms of the
German Free Software License

The License may be obtained under http://www.d-fsl.org.
***

If you like my work, I would appreciate a donation. You can find several options on my website www.dsoft-app-dev.de at section crowdfunding. Thank you!

This application depends on some Python modules, which should be installed into the virtual environment the WEB application is running from within. For the development system use:

    $ workon <name of virtualenv>
    $ pip install -r requirements/development.txt

For the production system use:

    $ workon <name of virtualenv>
    $ pip install -r requirements/production.txt

To create the database structure and group permissions use:

    $ ./manage.py migrate
    $ ./manage.py create_editors_permissions

To import sample data, use:

    $ ./manage.py loaddata .development/mysql/sample_data.json

To dump the whole database use:

    $ ./manage.py dumpdata --indent 2 > .development/mysql/dump.json

To dump a single database table use:

    $ ./manage.py dumpdata --indent 2 mywebsite_home.websitesettings > .development/mysql/dump_websettings.json

It is possible to exclude unneccessary tables during the database dump, e.g.:

    $ ./manage.py dumpdata --natural-foreign --natural-primary --indent 2 -e filebrowser -e reversion -e hitcount -e sessions -e admin -e captcha -e contenttypes -e auth -e dashboard -e members  > .development/mysql/sample_data.json

The app 'extendcmds' allows to create a superuser with a given password:

    $ ./manage.py createsuperuser --username <account name> --email <email> --password <secret password> --preserve --noinput

To generate a new SECRET_KEY use:

    $ function django_secret() { python -c "import random,string;print(''.join([random.SystemRandom().choice(\"{}{}{}\".format(string.ascii_letters, string.digits, string.punctuation)) for i in range(63)]).replace('\\'','\\'\"\\'\"\\''))"; }
    $ echo "DJANGO_SECRET_KEY='$(django_secret)'"

## Using the celery task system

For more information and a quick example, see https://simpleisbetterthancomplex.com/tutorial/2017/08/20/how-to-use-celery-with-django.html

## Adding celery tasks

To registry some tasks to celery, add the following lines to your settings file:

    from celery.schedules import crontab
    ..
    CELERY_BROKER_URL = 'amqp://localhost'
    CELERY_RESULT_BACKEND = 'amqp://localhost'
    CELERY_ACCEPT_CONTENT = ['application/json']
    CELERY_RESULT_SERIALIZER = 'json'
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_TIMEZONE = 'Europe/Berlin'
    CELERY_IMPORTS = 'test_website.tasks'
    CELERY_BEAT_SCHEDULE = {
        'test_task': {
            'task': 'test_website.tasks.test_task',
            'schedule': crontab(minute='*/1', hour='*')
        }
    }

To run the periodic scheduler, use the following command in your test environment:

    $ celery -A mywebsite_celery worker -l info -B

## Settings for mywebsite_home app

The mywebsite_home app represents the website with its index page. In order to get the correct pages for menu entries 'Crowdfunding', 'About', 'Imprint' and 'Data privacy', you have to create 3 article pages in the mywebsite_blog app and name the slug field with the names 'crowdfunding', 'about-me', 'imprint' and 'data-privacy'.

## Unit tests

Unit tests for Javascript is done by using QUnit, see https://qunitjs.com
To execute tests, you have 2 possibilities:

1) Run "python -m http.server" instead of "./manage.py runserver" from command line and browse http://localhost:8000/test/tests.html

or

2) Install QUnit (npm install -g qunit) and simply run it from command line "qunit"

All other unit tests can be run with

    $ tox

## More MyWebsite modules

* mywebsite_base
* mywebsite_members

## Build package

To build package via setup.py use:

    python setup.py sdist --formats=zip --dist-dir .tox/dist

## VSCode custom configuration

Custum configuration files are stored in the .vscode folder under the project path. The file *settings.json* contains
full path settings to programs, e.g. python, pep8 and so forth. It is recommended to exclude this file in git, using
*.gitignore*. Here is a template for *settings.json*:

    {
        "python.pythonPath": "<path to bin/python>",
        "python.linting.pep8Path": "<path to bin/pep8>",
        "python.linting.pep8Args": [
            "--max-line-length=119"
        ],
        "python.linting.pep8Enabled": true,
        "python.formatting.autopep8Path": "<path to bin/autopep8>",
        "python.formatting.autopep8Args": [
            "--max-line-length=119"
        ],
        "python.linting.pylintPath": "<path bin/pylint>",
        "python.linting.pylintArgs": [
            "--errors-only",
            "--load-plugins",
            "pylint_django"
        ],
        "python.linting.pylintEnabled": true,
        "html.format.enable": false,
        "eslint.enable": false,
        "editor.formatOnSave": true,
        "editor.rulers": [
            119,
            140
        ],
        "files.exclude": {
            "**/.git": true,
            "**/.svn": true,
            "**/.hg": true,
            "**/CVS": true,
            "**/.DS_Store": true,
            ".vscode": false,
            ".idea": true,
            "**/*,pyc": true,
            "*/__pycache__": true
        },
        "workbench.editor.enablePreview": false,
        "editor.find.globalFindClipboard": true,
        "search.globalFindClipboard": true,
        "editor.minimap.side": "left",
        "git.promptToSaveFilesBeforeCommit": true,
        "markdown-pdf.type": [
            "html",
        ],
        "markdown-pdf.convertOnSave": true,
        "markdown-pdf.outputDirectory": "templates/toods/admin_doc",
        "markdown-pdf.convertOnSaveExclude": [
            "^(?!MANUAL.*\\.md$)",
        ],
        "markdown-pdf.includeDefaultStyles": false,
        "markdown-pdf.styles": [
            "static/css/bootstrap.min.css",
        ],
    }
