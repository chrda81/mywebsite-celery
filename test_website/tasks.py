# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from datetime import datetime
from celery import shared_task


@shared_task
def test_task():
    print('Execute test_task at: {}'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')))
