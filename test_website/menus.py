# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------

Hint: django-simple-menu cannot handle localization. As work-around we use our own
      templatetags function `generate_simple_menu` to reverse() the urls. In addition
      we need a kwargs 'reverse_url' = 'namespace:url' for that.
"""

from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem

# Define children for the CATEGORIES menu
blog_children = (
    MenuItem(_("MENU_CATEGORIES"),
             reverse('blog:category'),
             weight=10,
             kwargs={'reverse_url': 'blog:category'}),
)

# Define children for the IMPRINT menu
imprint_children = (
    MenuItem(_("MENU_DATA_PRIVACY"),
             reverse('home:data-privacy'),
             weight=10,
             kwargs={'reverse_url': 'home:data-privacy'}),

    MenuItem(_("MENU_TERMS"),
             reverse('home:terms-and-conditions'),
             weight=20,
             kwargs={'reverse_url': 'home:terms-and-conditions'}),

    MenuItem(_("MENU_CONTACT"),
             reverse('home:contact'),
             weight=30,
             kwargs={'reverse_url': 'home:contact'}),
)

# Add items to our main menu
Menu.add_item("main", MenuItem(_("MENU_HOME"),
                               reverse('home:index'),
                               weight=10,
                               kwargs={'reverse_url': 'home:index'}))

Menu.add_item("main", MenuItem(_("MENU_BLOG"),
                               reverse('blog:blog'),
                               children=blog_children,
                               weight=30,
                               kwargs={'reverse_url': 'blog:blog'}))

Menu.add_item("main", MenuItem(_("MENU_ABOUT"),
                               reverse('home:about-me'),
                               weight=40,
                               kwargs={'reverse_url': 'home:about-me'}))

Menu.add_item("main", MenuItem(_("MENU_CROWDFUNDING"),
                               reverse('home:crowdfunding'),
                               weight=50,
                               kwargs={'reverse_url': 'home:crowdfunding'}))

Menu.add_item("main", MenuItem(_("MENU_IMPRINT"),
                               reverse('home:imprint'),
                               children=imprint_children,
                               weight=60,
                               kwargs={'reverse_url': 'home:imprint'}))
