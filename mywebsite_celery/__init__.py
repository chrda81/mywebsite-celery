# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from .celery import app as celery_app

__all__ = ['celery_app']
__version__ = '1.0.1'  # this version variable will be used in setup.py
default_app_config = 'mywebsite_celery.apps.CeleryConfig'
