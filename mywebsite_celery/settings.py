# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from mywebsite.settings.settings import *
try:
    from test_website.settings.settings import *
except ImportError:
    pass
try:
    from custom_website.settings.settings import *
except ImportError:
    pass
